# ps1 geomancy module, adds a fancy line for bash

# get current branch in git repo
parse_git_branch () {
	BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${BRANCH}" = "" ]
	then
		STAT=$(parse_git_dirty)
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
parse_git_dirty () {
	status=$(git status 2>&1 | tee)
	dirty=$(printf "%s" "${status}" 2> /dev/null | grep -qs "modified:"; echo "$?")
	untracked=$(printf "%s" "${status}" 2> /dev/null | grep -qs "Untracked files"; echo "$?")
	ahead=$(printf "%s" "${status}" 2> /dev/null | grep -qs "Your branch is ahead of"; echo "$?")
	newfile=$(printf "%s" "${status}" 2> /dev/null | grep -qs "new file:"; echo "$?")
	renamed=$(printf "%s" "${status}" 2> /dev/null | grep -qs "renamed:"; echo "$?")
	deleted=$(printf "%s" "${status}" 2> /dev/null | grep -qs "deleted:"; echo "$?")
	bits=''
	if [ "${renamed}" = "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" = "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" = "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" = "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" = "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" = "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" = "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

nonzero_return () {
	RETVAL=$?
	[ $RETVAL -ne 0 ] && echo "$RETVAL"
}

export PS1="\A [\[\e[33m\]\`nonzero_return\`\[\e[m\]] \u@\H:\[\e[32m\]\w\[\e[m\] \[\e[34m\]\`parse_git_branch\`\[\e[m\] \\n\$ "
