# alias module for geomancy, sets a number of useful aliases

alias ls='ls --color=always --group-directories-first'
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
