# About

geomancy is a collection of bash and vim configurations plus an install/removal
script to plop into new environments like one would crack an egg into instant
ramen. Currently it installs everything with zero options. These are opinionated
components that I'm personally using, Feel free to fork and add your own modules
if the defaults are not suitable.

Included configuration files:

* bash
  * ps1 - a fancier prompt
  * alias
* vim
* redshift (location set to manual for now)
* i3
* kitty
* nvm

Golden rule: no dependencies required beyond whatever software you need fiddled 
with. Ain't no `npm`s or `cargo`s required unless I end up including a config 
specifically for them.

# Dependencies

* git
* bash - `bash`, not `sh`, or `ash`, or... I think it's `fish` and `zsh` portable, but haven't tested yet.
* technically jq, but I package it as a local executable and delete it when
  installation finishes

# Installation

Don't go running bash scripts from weird git repos, check the file first!

```bash
wget https://gitlab.com/thepiper/geomancy/raw/master/geomancy.sh > ./geomancy.sh
less ./geomancy.sh
```

### screw safety I want my egg ramen

#### curl gurls
```bash
curl -sSL https://gitlab.com/thepiper/geomancy/raw/master/geomancy.sh -o ./geomancy.sh && bash ./geomancy.sh install
```

#### wget baguettes
```bash
wget https://gitlab.com/thepiper/geomancy/raw/master/geomancy.sh && bash ./geomancy.sh install
```

# How it Works

geomancy does two major things: inject bash dependencies into .bashrc through
chainloading, and sets up config files. These two tasks are done for every
module included. 

### `.bashrc` Injection

Done through sourcing a main `geo.sh` file via `.bashrc`, then setting `geo.sh`
to further load bash injections for each individual module that is installed.

### Config Setup

Config file paths for each module supported are tracked through `soil.json`.
Installation will backup existing configs and replace with new ones. Currently
only does replacement (versus appending). So if you want to use your own files,
use configs that are complete.

### jq

Is used to parse the module list database. It's foss, but comes with some
license inclusion requirements, so I chose to fetch a copy everytime it is
needed. I will eventually move away from it, since the rest of geomancy is
public domain.


# Uninstall

Geomancy installs itself to the `$GEO` folder (by default
`~/.config/geomancy/`). It comes with an uninstall script or you can try to
manuall remove it.

### Script
The installer places a copy of `geomancy.sh` in `$HOME/.config/geomancy/` run
`bash geomancy.sh remove` will start up the automatic removal process. Currently
the uninstaller leaves directory stubs because I'm lazy and didn't bother to
code in fully removable relative directories.

### Manual removal
Alternatively, look into `~/.config/geomancy/soil.json` to see where config
files were injected, and remove/release them. Backups of existing configs are
stored in `.config/geomancy/backups/` in relative folder sturcture (referenced to
`~/`).

# Stable vs Master

stable is like debian stable, master is usually working but sometimes not,
develop is 99% not working, since when it works it just gets bumped to master

# Licensing

![creative-commons 0][cc0]

All code made available here in source form has been written by James Liu and is
released to the public domain, assisted by CC0. Executables or libraries used 
will have their respective licensing listed under the `/license` directory

[cc0]:https://licensebuttons.net/p/zero/1.0/88x31.png