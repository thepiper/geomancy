#!/usr/bin/env bash

export PATH="$PATH:/home/james/.emacs.d/bin"
export PATH="$PATH:/home/james/.cargo/bin"
export PATH="$PATH:/home/james/tooling/flutter/bin"
