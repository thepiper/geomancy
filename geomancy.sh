#!/bin/bash

export BRANCH='master'
export _INSTALL="geo_temp"
export _GEO=".config/geomancy"
export _MANCY="geo.sh"
export _CONF=".config"
export _GEO_VIM="geo.vim"
export _GEO_KITTY="geo.kty"
export _BASH_BAK="bashrc.bak"
export _SOIL="soil.json"
export _JQ="$_INSTALL/jq"


geo_error() {
    printf "ERROR: %s\n" "$1"
    exit 1
}


list_gen(){

  if [ $# == 0 ]
  then
    ROOT=$_INSTALL
  else
    ROOT=$1
  fi

  touch "$ROOT/$_SOIL"                   # make sure the actual file exists in lieu of file not found erros

  # jq witchcraft - pulls the keylist (i.e. list of mods) from the mod db, then formats the output into bash array via sed
  #echo "$_JQ"
  MODLIST=$($_JQ -j 'keys_unsorted' $ROOT/$_SOIL | sed '1 d; $ d; s/  //g; s/\,//; s/\"//g')

  echo "Detected modules:"
  echo ${MODLIST[*]} #DEBUG?

  return
}

install() {

  # dump the "install" arg to extract the requested mod list
  shift

  # check for and ensure existance of .config folder in home
  if [ ! -f "$_CONF" ]
  then
      mkdir ./"$_CONF"
  fi

  if [ ! -f "./$_GEO" ]
  then
      mkdir ./"$_CONF"/geomancy
  fi

  mkdir $_GEO/backups/

  
  # clone from geomancy repo into a temp directory
  git clone -b $BRANCH https://gitlab.com/thepiper/geomancy.git $_INSTALL

  # get MODLIST
  list_gen


  # check for stray weirdly typed mods, use passed list if checks out
  if [ $# != 0 ]
  then
    for MOD in "$@"; do
      if [[ $($_JQ 'has("$MOD")' $_INSTALL/$_SOIL) == false ]]
      then
        geo_error "Could not find $MOD in geomantic vaguaries - it is likely not yet supported."
        return
      fi
    done
    # reassign MODlist if all requested mods present
    MODLIST=$@
  fi

  # making sure bashrc exists before we start fiddling with anything else
  touch ./.bashrc

  # backup bashrc before any modifications are made
  cp .bashrc ./$_GEO/$_BASH_BAK

  # set up geo's tech and setting it to source when started
  cp ./$_INSTALL/geo.sh ./$_GEO/$_MANCY

  # bashrc injection of geo.sh so bashrc remains otherwise untouched
  printf "\n# geomancy setup\nexport GEO=\"%s/$_GEO\"\n[ -f \"\$GEO/geo.sh\" ] && . \$GEO/geo.sh\n" "$HOME" >> ./.bashrc

  echo "Generating local modlist"

  # create an installed modlist to keep track of installed mods
  touch ./$_GEO/$_SOIL

  printf "{\n" >> ./$_GEO/$_SOIL

  # pop the installed mods from the package list into the list of installed packages
  for MOD in $MODLIST; do
    $_JQ ."$MOD" $_INSTALL/$_SOIL | sed "1 s/^/\"$MOD\" : /; s/^/  /; $ s/$/,\n/" >> ./$_GEO/$_SOIL
    # DEBUG
    #cmd $($_JQ ."$MOD" $_INSTALL/$_SOIL)
    #cat ./$_GEO/$_SOIL
    #echo ++++++++++
  done
  # remove empty lines, and an initial comma
  sed -i '$d' ./$_GEO/$_SOIL
  sed -i '$ s/\,//' ./$_GEO/$_SOIL
  printf "}\n" >> ./$_GEO/$_SOIL

  echo "New modlist generated:"
  cat ./$_GEO/$_SOIL
  echo "stored at $PWD/$_GEO/$_SOIL"
  # actual installation of modules
  for MOD in $MODLIST; do
    echo "Installing $MOD modules."

    if [[ $($_JQ ."$MOD".bash $_INSTALL/$_SOIL) == 1 ]]
    then
      echo "Found bash injections for $MOD, copying into $_GEO"
      # check and install bash injections required
      # - absolute paths are used here for consistency
      cp "./$_INSTALL/$MOD.sh" "./$_GEO/"
      printf "[ -f \"$HOME/$_GEO/%s.sh\" ] && . $HOME/$_GEO/%s.sh\n" "$MOD" "$MOD" >> "./$_GEO/$_MANCY"
    fi

    # check if a config directory exists
    if [[ $($_JQ ."$MOD".dir $_INSTALL/$_SOIL) != null ]]
    then
      # load the config path from db, then strip quotes
      CONFDIR_RAW=$($_JQ ."$MOD".dir $_INSTALL/$_SOIL)
      CONFDIR=$(sed -e 's/^"//' -e 's/"$//' <<< "$CONFDIR_RAW")
      echo "Installing config files for $MOD"

      # DEBUG
      # $_JQ ."$MOD".files $_INSTALL/$_SOIL | tr '",' ' ' | sed -e 's/\[/(/; s/\]/)/;' | sed -e 's/[[:space:]]//g; /^(/d; /^)/d'

      # Uses jq to generate a newline separated stream of file names that are re
      # quired by the mod, copy each file from the path under the install root
      # to the same relative path under  $HOME
      A=0 # good old loop counter
      for CONFFILE in $($_JQ ."$MOD".files $_INSTALL/$_SOIL | tr '",' ' ' | sed -e 's/\[/(/; s/\]/)/;' | sed -e 's/[[:space:]]//g; /^(/d; /^)/d'); do
        echo "Found $CONFFILE, checking for conflicts..." #DEBUG
        # sanity check for existance of the config, otherwise do nothing
        if [[ -f "./$_INSTALL/config_root/$CONFDIR/$CONFFILE"  ]]
        then
          # if the config file already exists, make a backup
          if [[ -f "./$CONFDIR/$CONFFILE" ]]
          then
            echo "Conflict present, backing up ./$CONFDIR/$CONFFILE to ./$_GEO/backups/"
            mkdir -p "./$_GEO/backups/$CONFDIR"
            cp "./$CONFDIR/$CONFFILE" "./$_GEO/backups/$CONFDIR"
          elif [[ ! -d "./$CONFDIR" ]] # if the conf directory isn't there, pop it in
          then
            echo "Config site ./$CONFDIR not found, generating..."
            mkdir -p "./$CONFDIR"
          fi

          # final copy of the config
          echo "Conflicts dealt with appropriately"
          echo "Copying ./$_INSTALL/config_root/$CONFDIR/$CONFFILE to ./$CONFDIR/$CONFFILE"
          cp "./$_INSTALL/config_root/$CONFDIR/$CONFFILE" "./$CONFDIR"
        fi
        (( A++ ))
      done
    fi
  done

  # cleanup after
  echo "Cleaning house."
  rm -rf ./$_INSTALL

  mv ./geomancy.sh ./$_GEO/geomancy.sh

  return
}

remove() {
  (( ERRORS=0 ))

  echo "Fetching jq from geomantic cloud"
  wget https://gitlab.com/thepiper/geomancy/-/raw/master/jq

  export _JQ="./jq"

  if [ -f "./$_GEO/$_MANCY" ]
  then
    echo "Removing geomancy."

    list_gen "./$_GEO"

    echo ${MODLIST[*]}

    # restore mods from backup
    for MOD in $MODLIST; do
      # load the config path from db, then strip quotes
      CONFDIR_RAW=$($_JQ ."$MOD".config $_GEO/$_SOIL)
      CONFDIR=$(sed -e 's/^"//' -e 's/"$//' <<<"$CONFDIR_RAW")

      echo "Restoring $MOD from backup."

      for CONFFILE in $($_JQ ."$MOD".files $_INSTALL/$_SOIL | tr '",' ' ' | sed -e 's/\[/(/; s/\]/)/;' | sed -e 's/[[:space:]]//g; /^(/d; /^)/d'); do
        if [[ -f ./$_GEO/backups/$CONFDIR/$CONFFILE ]] # restore from backups if exists
        then
          cp -i "./$_GEO/backups/$CONFDIR/$CONFFILE" "./$CONFDIR/$CONFFILE"
        else
          while [ true ]; do
            echo "Could not find config backup for $MOD, nuke? (Y/n)"
            read -r OPTION
            case $OPTION in
              "n"|"N"|"No"|"no"|"NO")
                CONFDIR=`dirname $CONFDIR_RAW`
                mkdir -p "./geo_backups/$CONFDIR"
                cp -R "$CONFDIR_RAW" "./geo_backups/$CONFDIR"
                DONE=true;
                ;;
              ""|"y"|"Y"|"yes"|"Yes")
                DONE=true;
                rm -r "$CONFDIR_RAW"
                ;;
              *)
            esac

            if [ $DONE ]
            then
              break
            fi
          done
        fi
      done
    done

    echo 'Restoring bashrc from backup.'
    REMOVE=$(cat ./$_GEO/$_BASH_BAK > ./.bashrc)

    # error check the removal operation - $? stores the exit code
    if [[ ! "$?" ]]
    then
      cp -r ./$_GEO/$_BASH_BAK "$HOME"
      geo_error "Could not remove bashrc, moving backup to $HOME"
      (( ERRORS+=1 ))
    fi

    REMOVE=$(rm -r ./$_GEO)
    echo "$REMOVE"

    if [[ ! "$?" ]]
    then
      geo_error "Could not remove $_GEO, try removing manually instead."
      (( ERRORS+=1 ))
    fi
  fi

  if [ ! "$ERRORS" ]
  then
    printf "Unable to succesffully remove geomancy with %s errors\n" "$ERRORS"
    exit 1
  else
    printf "Successfully removed geomancy.\n"
    exit 0
  fi
}

# DEBUG: keep the line commented for debugging
 cd "$HOME" || geo_error "Can't find $HOME directory."

if [ "$#" == "1" ]
then
  case $1 in
    "install")
      install "all"
      ;;
    "remove")
      remove "all"
      ;;
    "")
      if [ ! -f ./$_GEO ]
      then install
      else echo "Geomancy already installed, exiting."
      fi;
      ;;
    *)
      echo "Unknown command '$1', please try again"
      ;;
  esac
else
  case $1 in
    "install")
      shift
      install "$@"
      ;;
    "remove")
      shift
      remove "$@"
      ;;
    *)
      echo "Unknown command '$1', please try again"
      exit 1
      ;;
  esac
fi
